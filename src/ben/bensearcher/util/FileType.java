package ben.bensearcher.util;

public enum FileType {
	ALL,
	PDF,
	TXT
}
