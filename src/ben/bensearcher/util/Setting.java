package ben.bensearcher.util;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public final class Setting {
	
	private static final String PROPERTIES_FILE_NAME = "bensearcher.setting";
	private static final String PROPERTIES_FILE_RESSOURCE = "ben/bensearcher/util/"+PROPERTIES_FILE_NAME;
	private static final String PROPERTY_FILE_TYPE = "file_type";
	private static final String PROPERTY_ROOT_PATH = "root_path";
	private static final String PROPERTY_RESULT_LIMIT = "result_limit";
	
	private static StringProperty fileType;
	private static StringProperty rootPath;
	private static IntegerProperty resultLimit;
	
	
	private Setting() throws Exception{
		if(fileType == null || rootPath == null || resultLimit == null){
			Properties properties = new Properties();
			//ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			//InputStream propertiesFile = classLoader.getResourceAsStream( PROPERTIES_FILE_RESSOURCE );
			InputStream propertiesFile = getClass().getResourceAsStream(PROPERTIES_FILE_NAME);
			if ( propertiesFile == null ) {
				throw new Exception( "Can't find properties file : " + PROPERTIES_FILE_RESSOURCE  );
			}
			
			fileType = new SimpleStringProperty();
			rootPath = new SimpleStringProperty();
			resultLimit = new SimpleIntegerProperty();
			
			try {
				properties.load( propertiesFile );
				fileType.set(properties.getProperty( PROPERTY_FILE_TYPE ));
				rootPath.set(properties.getProperty( PROPERTY_ROOT_PATH ));
				resultLimit.set(Integer.valueOf( properties.getProperty(PROPERTY_RESULT_LIMIT )));
				File file = new File(rootPath.get());
				if(!file.exists() || !file.isDirectory()){
					rootPath.set( AppConst.HOME_DIR );
				}
			} catch ( Exception e ) {
				throw new Exception( "Can't load properties file : " + PROPERTIES_FILE_RESSOURCE, e );
			}
		}
	}


	public final StringProperty fileTypeProperty() {
		return fileType;
	}
	


	public final FileType getFileType() {
		return FileType.valueOf( fileTypeProperty().get() );
	}
	


	public final void setFileType(FileType fileType) {
		this.fileTypeProperty().set(fileType.name());
	}
	


	public final StringProperty rootPathProperty() {
		return rootPath;
	}
	


	public final String getRootPath() {
		return this.rootPathProperty().get();
	}
	


	public final void setRootPath(final String rootPath) {
		this.rootPathProperty().set(rootPath);
	}
	


	public final IntegerProperty resultLimitProperty() {
		return resultLimit;
	}
	


	public final int getResultLimit() {
		return this.resultLimitProperty().get();
	}
	


	public final void setResultLimit(final int resultLimit) {
		this.resultLimitProperty().set(resultLimit);
	}
	
	
	
	public boolean save(){
		Properties properties = new Properties();
		//ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		//InputStream propertiesFile = classLoader.getResourceAsStream( PROPERTIES_FILE_RESSOURCE );
		InputStream propertiesFile = getClass().getResourceAsStream(PROPERTIES_FILE_NAME);
		if ( propertiesFile == null ) {
			return false;
		}
		
		try {
			properties.load( propertiesFile );
			properties.setProperty( PROPERTY_FILE_TYPE , fileType.get() );
			properties.setProperty( PROPERTY_ROOT_PATH , rootPath.get() );
			properties.setProperty( PROPERTY_RESULT_LIMIT , String.valueOf(resultLimit.get() ) );
			//File res = new File( classLoader.getResource(PROPERTIES_FILE_RESSOURCE).getPath() );
			File res = new File( getClass().getResource(PROPERTIES_FILE_NAME).getPath());
			OutputStream out = new FileOutputStream(res);
			properties.store(out, AppConst.APP_NAME);
		} catch ( IOException e ) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	public static Setting load() throws Exception{
		return new Setting();
	}
	
	public void reload() throws Exception{
		new Setting();
	}
	
	
	private static boolean toXML(Setting setting){
		XMLEncoder encoder = null;
		
		try{
			encoder = new XMLEncoder(
							new BufferedOutputStream(
								new FileOutputStream(Setting.PROPERTIES_FILE_NAME)));
			encoder.writeObject(setting);
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(encoder != null)
				encoder.close();
		}
		
		return true;
	}
	
	
	
	private static Setting fromXML(){
		XMLDecoder decoder = null;
		Setting setting = null;
		try{
			decoder = new XMLDecoder(
							new BufferedInputStream(
								new FileInputStream(Setting.PROPERTIES_FILE_NAME)));
			setting = (Setting) decoder.readObject();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(decoder != null)
				decoder.close();
		}
		
		return setting;
	}

}
