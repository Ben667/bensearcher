package ben.bensearcher.util;

import java.io.File;

public class AppConst {
	
	//	String constants
	public static final String APP_NAME = "BenSearcher";
	public static final File RECENT_FILE = new File( AppConst.class.getResource("bensearcher.rencent").getPath() );
	public static final String HOME_DIR = System.getProperty("user.home");
	public static final String USER_NAME = System.getProperty("user.name");
	
	//	Integer constants
	public static final int MAX_RECENT_SEARCHES = 5;
}
