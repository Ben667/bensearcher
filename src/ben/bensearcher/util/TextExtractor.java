package ben.bensearcher.util;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.jpedal.PdfDecoder;
import org.jpedal.exception.PdfException;
import org.jpedal.exception.PdfSecurityException;
import org.jpedal.grouping.PdfGroupingAlgorithms;
import org.jpedal.objects.PdfPageData;
import org.jpedal.utils.Strip;

import ben.bensearcher.bean.Document;


@SuppressWarnings("unused")
public class TextExtractor {


	
	public static StringBuilder extract(Document doc, String keyword) throws Exception{
		String error = "Can't read "+doc.getPath()+" file: ";
		if(doc.getFileType() == null){
			throw new Exception(error+"unknow file type.");
		}
		switch(doc.getFileType()){
			case PDF:
				return extractWordsFromPDF(doc.getPath(),keyword);
			case TXT:
				return extractTextFormTextFile(doc.getPath());
			default:
				throw new Exception(error+"unknow file type.");
		}
	}
	
	
	
	
	
	
	
	
	
	private static StringBuilder extractTextFormTextFile(String file_name) throws IOException{
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(new File(file_name));
		StringBuilder text = new StringBuilder();
		while(scanner.hasNext()){
			text.append(scanner.next()+" ");
		}
		return text;
	}
	
	
	
	
	
	
		// ----------------------------------------------------------
		// --- extract WORDS from a PDF file using JPEDAL
		// ----------------------------------------------------------
		/**
		 * @param file_name the name of pdf file
		 * @return a StringBuilder of all pdf text 
		 * @throws IOException
		 */
	@SuppressWarnings("rawtypes")	
	private static StringBuilder extractWordsFromPDF(String file_name) throws IOException{
		  PdfDecoder decodePdf = null;

		  StringBuilder wordsBuffer = new StringBuilder();

		  // --- if XML content not require, then *pure text* extraction is much faster
		  PdfDecoder.useTextExtraction();
				
		  try
		  {
		    decodePdf = new PdfDecoder(false);
		    decodePdf.setExtractionMode(PdfDecoder.TEXT); // --- extract just text
		    PdfDecoder.init(true);

		    // --- reset to use unaltered co-ords - allow use of rotated or unrotated
		    //     co-ordinates on pages with rotation (used to be in PdfDecoder)
		    PdfGroupingAlgorithms.useUnrotatedCoords = false;
					
		    decodePdf.openPdfFile(file_name);
		  }
		  catch (PdfSecurityException e) {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name);}
		  catch (PdfException e)         {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name);}
		  catch (Exception e)            {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name); e.printStackTrace();}
			
		  // --- extract data from pdf (if allowed)
		  if(!decodePdf.isExtractionAllowed()) ;//System.out.println("Text extraction from " + file_name + " not allowed!");
		  else if (decodePdf.isEncrypted() && !decodePdf.isPasswordSupplied()) System.out.println("Encrypted settings in " + file_name + "!");
		  else
		  {
		    int start = 1, end = decodePdf.getPageCount(); // --- page range: all pages

		    // --- extract data from pdf
		    try
		    {
		      for (int page = start; page < end + 1; page++)
		      {
		        decodePdf.decodePage(page);

		        // --- create a grouping object to apply grouping to data
		        PdfGroupingAlgorithms currentGrouping = decodePdf.getGroupingObject();

		        // --- use whole page size
		        PdfPageData currentPageData = decodePdf.getPdfPageData();

		        // --- co-ordinates are x1,y1 (top left hand corner), x2,y2 (bottom right)
		        int x1 = currentPageData.getMediaBoxX(page);
		        int x2 = currentPageData.getMediaBoxWidth(page) + x1;

		        int y2 = currentPageData.getMediaBoxX(page);
		        int y1 = currentPageData.getMediaBoxHeight(page) - y2;
							
		        List words = null;
		        
		        try
		        {
		          //true mean and the above chars will be ignored in table of content
		          words = currentGrouping.extractTextAsWordlist(x1, y1, x2, y2, page, true, "&:=()!;.,\\/\"\"\'\'");
		        }
		        catch (PdfException e)
		        {
		          decodePdf.closePdfFile();
		          //System.err.println("Exception = " + e + " in " + file_name);
		        }

		        if (words != null) ;//System.out.println("No text found in " + file_name + "!");
		        {
		          Iterator wordIterator = words.iterator();
		          while ( wordIterator.hasNext() )
		          {
		            String currentWord = (String) wordIterator.next();
										
		            // --- remove the XML formatting if present - not needed for pure text
		            currentWord = Strip.convertToText(currentWord);
										
					int wx1 = (int) Float.parseFloat((String) wordIterator.next());
		            int wy1 = (int) Float.parseFloat((String) wordIterator.next());
		            int wx2 = (int) Float.parseFloat((String) wordIterator.next());
		            int wy2 = (int) Float.parseFloat((String) wordIterator.next());
									
		            wordsBuffer.append(currentWord + " ");	
		          }
		        }

		        // --- remove data once written out
		        decodePdf.flushObjectValues(false);
		      }
		    }
		    catch (Exception e)
		    {
		      decodePdf.closePdfFile();
		      //System.err.println("Exception " + e + " in " + file_name);
		    }

		    // --- flush data structures - not strictly required
		    decodePdf.flushObjectValues(true);

		    //System.out.println("\nText read from " + file_name + ".");
		  }
		  
		  decodePdf.closePdfFile();
		  decodePdf = null;

		  return wordsBuffer;
		}
		// ----------------------------------------------------------

	
	
	
	
	
	
	
	
	
	
	// ----------------------------------------------------------
	// --- extract WORDS from a PDF file using JPEDAL -- customized
	// ----------------------------------------------------------
	/**
	 * @param file_name the name of pdf file
	 * @return a StringBuilder of all pdf text 
	 * @throws IOException
	 */
@SuppressWarnings("rawtypes")	
private static StringBuilder extractWordsFromPDF(String file_name, String keyword) throws IOException{
	  PdfDecoder decodePdf = null;

	  StringBuilder wordsBuffer = new StringBuilder();

	  // --- if XML content not require, then *pure text* extraction is much faster
	  PdfDecoder.useTextExtraction();
			
	  try
	  {
	    decodePdf = new PdfDecoder(false);
	    decodePdf.setExtractionMode(PdfDecoder.TEXT); // --- extract just text
	    PdfDecoder.init(true);

	    // --- reset to use unaltered co-ords - allow use of rotated or unrotated
	    //     co-ordinates on pages with rotation (used to be in PdfDecoder)
	    PdfGroupingAlgorithms.useUnrotatedCoords = false;
				
	    decodePdf.openPdfFile(file_name);
	  }
	  catch (PdfSecurityException e) {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name);}
	  catch (PdfException e)         {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name);}
	  catch (Exception e)            {}//System.err.println("Exception " + e + " in pdf code for wordlist" + file_name); e.printStackTrace();}
		
	  // --- extract data from pdf (if allowed)
	  if(!decodePdf.isExtractionAllowed()) ;//System.out.println("Text extraction from " + file_name + " not allowed!");
	  else if (decodePdf.isEncrypted() && !decodePdf.isPasswordSupplied()) System.out.println("Encrypted settings in " + file_name + "!");
	  else
	  {
	    int start = 1, end = decodePdf.getPageCount(); // --- page range: all pages
	    //readAgain is used for stopping when keyword has been found
	    // --- extract data from pdf
	    try
	    {
	      for (int page = start, readAgain = end; page < end + 1 && readAgain > 0; page++,readAgain--)
	      {
	        decodePdf.decodePage(page);

	        // --- create a grouping object to apply grouping to data
	        PdfGroupingAlgorithms currentGrouping = decodePdf.getGroupingObject();

	        // --- use whole page size
	        PdfPageData currentPageData = decodePdf.getPdfPageData();

	        // --- co-ordinates are x1,y1 (top left hand corner), x2,y2 (bottom right)
	        int x1 = currentPageData.getMediaBoxX(page);
	        int x2 = currentPageData.getMediaBoxWidth(page) + x1;

	        int y2 = currentPageData.getMediaBoxX(page);
	        int y1 = currentPageData.getMediaBoxHeight(page) - y2;
						
	        List words = null;
	        
	        try
	        {
	          //true mean and the above chars will be ignored in table of content
	          words = currentGrouping.extractTextAsWordlist(x1, y1, x2, y2, page, true, "&:=()!;.,\\/\"\"\'\'");
	        }
	        catch (PdfException e)
	        {
	          decodePdf.closePdfFile();
	          //System.err.println("Exception = " + e + " in " + file_name);
	        }

	        if (words != null) ;//System.out.println("No text found in " + file_name + "!");
	        {
	          Iterator wordIterator = words.iterator();
	          while ( wordIterator.hasNext() )
	          {
	            String currentWord = (String) wordIterator.next();
									
	            // --- remove the XML formatting if present - not needed for pure text
	            currentWord = Strip.convertToText(currentWord);
									
				int wx1 = (int) Float.parseFloat((String) wordIterator.next());
	            int wy1 = (int) Float.parseFloat((String) wordIterator.next());
	            int wx2 = (int) Float.parseFloat((String) wordIterator.next());
	            int wy2 = (int) Float.parseFloat((String) wordIterator.next());
								
	            wordsBuffer.append(currentWord + " ");	
	            
	            //if you find word
	            if(currentWord.contains(keyword) || currentWord.equals(keyword)){
	            	readAgain = 3;	//read 3 or 2 again and stop
	            }
	          }
	        }

	        // --- remove data once written out
	        decodePdf.flushObjectValues(false);
	      }
	    }
	    catch (Exception e)
	    {
	      decodePdf.closePdfFile();
	      //System.err.println("Exception " + e + " in " + file_name);
	    }

	    // --- flush data structures - not strictly required
	    decodePdf.flushObjectValues(true);

	    //System.out.println("\nText read from " + file_name + ".");
	  }
	  
	  decodePdf.closePdfFile();
	  decodePdf = null;

	  return wordsBuffer;
	}
	// ----------------------------------------------------------

}
