package ben.bensearcher;
	
import java.io.IOException;

import ben.bensearcher.util.AppConst;
import ben.bensearcher.view.MessageBox;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class MainApp extends Application {
	

    @Override
    public void start(Stage primaryStage) {
    	
        primaryStage.setTitle(AppConst.APP_NAME);
	    try{
	        primaryStage.getIcons().add(new Image(MainApp.class.getResourceAsStream("view/icon.ico")));
	    }catch(NullPointerException e){}
        
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/appView.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout,720,575);
            
            primaryStage.setScene(scene);
            
            primaryStage.setOnCloseRequest(e -> {
            	String title = AppConst.APP_NAME + " - Exit";
        		String message = "Do you really want to exit ?";
        		if(MessageBox.confirm(title, message)){
        			Platform.exit();
        		}else{
        			e.consume();
        		}
            });
            
            primaryStage.show();
           
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    


    public static void main(String[] args) {
        launch(args);
    }
    
	
}
