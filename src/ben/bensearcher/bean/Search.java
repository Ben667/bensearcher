/**
 * 
 */
package ben.bensearcher.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;

import ben.bensearcher.util.FileType;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *Bean class for Searches.
 *
 */
@SuppressWarnings("serial")
public class Search implements Serializable {

	
	private Long id;
	private StringProperty motif;
	private ObjectProperty<FileType> fileType;
	private StringProperty rootPath;
	private IntegerProperty resultLimit;
	private ObjectProperty<Date> datetime;
	
	
	/**
	 * @param id
	 * @param motif
	 * @param fileType
	 * @param rootPath
	 * @param resultLimit
	 * @param datetime
	 * @throws BeanException 
	 */
	public Search(long id, String motif, FileType fileType, String rootPath,
			int resultLimit, Date datetime) throws BeanException {
		if(id > 0 && datetime != null && fileType != null){
			this.id = id;
			this.motif = new SimpleStringProperty(motif);
			this.fileType = new SimpleObjectProperty<FileType>(fileType);
			this.rootPath = new SimpleStringProperty(rootPath);
			this.resultLimit = new SimpleIntegerProperty(resultLimit);
			this.datetime = new SimpleObjectProperty<Date>(datetime);
		}else{
			throw new BeanException("id < 0 or datetime is null or fileType is null");
		}
	}
	
	
	public final long getID(){
		return id.longValue();
	}
	
	
	public void setID(long id){
		this.id = new Long(id);
	}
	
	public final StringProperty motifProperty() {
		return this.motif;
	}
	
	public final String getMotif() {
		return this.motifProperty().get();
	}
	
	public final void setMotif(final String motif) {
		this.motifProperty().set(motif);
	}
	
	public final ObjectProperty<FileType> fileTypeProperty() {
		return this.fileType;
	}
	
	public final FileType getFileType() {
		return this.fileTypeProperty().get();
	}
	
	public final void setFileType(final FileType fileType) {
		this.fileTypeProperty().set(fileType);
	}
	
	public final StringProperty rootPathProperty() {
		return this.rootPath;
	}
	
	public final String getRootPath() {
		return this.rootPathProperty().get();
	}
	
	public final void setRootPath(final String rootPath) {
		this.rootPathProperty().set(rootPath);
	}
	
	public final IntegerProperty resultLimitProperty() {
		return this.resultLimit;
	}
	
	public final int getResultLimit() {
		return this.resultLimitProperty().get();
	}
	
	public final void setResultLimit(final int resultLimit) {
		this.resultLimitProperty().set(resultLimit);
	}
	
	public final ObjectProperty<Date> datetimeProperty() {
		return this.datetime;
	}
	
	public final Date getDatetime() {
		return this.datetimeProperty().get();
	}
	
	public final void setDatetime(final Date datetime) {
		this.datetimeProperty().set(datetime);
	}
	
	
	public static Search createFromResultSet(final ResultSet r){
		try {
			Long id_ = r.getLong("id");
			String motif_ = r.getString("motif");
			FileType fileType_ = FileType.valueOf( r.getString("file_type") );
			String rootPath_ = r.getString("root_path") ;
			Integer resultLimit_= r.getInt("limit");
			Date datetime_ = new Date(r.getLong("datetime")); 
			return new Search(id_,motif_,fileType_,rootPath_,resultLimit_,datetime_);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public Search(){
		this.motif = new SimpleStringProperty();
		this.fileType = new SimpleObjectProperty<FileType>();
		this.rootPath = new SimpleStringProperty();
		this.resultLimit = new SimpleIntegerProperty();
		this.datetime = new SimpleObjectProperty<Date>();
	}

}
