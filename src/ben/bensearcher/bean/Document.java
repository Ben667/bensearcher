package ben.bensearcher.bean;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;

import ben.bensearcher.util.FileType;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Bean class for a Document, such as PDF,text, all kind readable by app.
 *
 */

@SuppressWarnings("serial")
public class Document implements Serializable {
	/**
	 * ID in database
	 */
	private Long id;
	
	/**
	 * path to this doc
	 */
    private StringProperty path;
    
    /**
     * count every time this doc appear in any result search
     */
    private IntegerProperty appear;
    
    /**
     * count every time this doc has been viewed
     */
    private IntegerProperty view;
    
    /**
     * The approximate length of doc
     */
    private IntegerProperty length;
    
    /**
     * Time who the doc was indexed
     */
    private ObjectProperty<LocalDateTime> indexedAt;
    
    
    private String name;
    
    
    
	/**
	 * @param id
	 * @param path
	 * @param appear
	 * @param view
	 * @param length
	 * @param indexedAt
	 * @throws BeanException 
	 */
	public Document(long id, String path, int appear, int view, int length, LocalDateTime indexedAt) throws BeanException {
		File file = new File(path);
    	if(file.exists()){
    		if( appear >= 0 && view >= 0 && length>=0){
    			if(indexedAt != null){
		    		this.name = file.getName();
					this.id = id;
					this.path = new SimpleStringProperty(path);
					this.appear = new SimpleIntegerProperty(appear);
					this.view = new SimpleIntegerProperty(view);
					this.length = new SimpleIntegerProperty(length);
					this.indexedAt = new SimpleObjectProperty<LocalDateTime> (indexedAt);
    			}else{
    				throw new BeanException("indexed at can't be null!");
    			}
    		}else{
    			throw new BeanException("appear, view or length can be less than zero(0)!");
    		}
    	}else{
    		throw new BeanException("The file "+path+" doesn't exist!");
    	}
	}


	public final long getID() {
		return this.id;
	}
	
	
	public final void setID(final int id) {
		this.id = new Long(id);
	}
	
	public final StringProperty pathProperty() {
		return this.path;
	}
	
	public final String getPath() {
		return this.pathProperty().get();
	}
	
	public final void setPath(final String path) {
		File f = new File(path);
		if(f.exists() && f.isFile()){
			this.pathProperty().set(path);
			this.name = f.getName();
		}
	}
	
	public final IntegerProperty appearProperty() {
		return this.appear;
	}
	
	public final int getAppear() {
		return this.appearProperty().get();
	}
	
	public final void setAppear(final int appear) {
		this.appearProperty().set(appear);
	}
	
	public final IntegerProperty viewProperty() {
		return this.view;
	}
	
	public final int getView() {
		return this.viewProperty().get();
	}
	
	public final void setView(final int view) {
		this.viewProperty().set(view);
	}
	
	public final IntegerProperty lengthProperty() {
		return this.length;
	}
	
	public final int getLength() {
		return this.lengthProperty().get();
	}
	
	public final void setLength(final int length) {
		this.lengthProperty().set(length);
	}
	
	public final ObjectProperty<LocalDateTime> indexedAtProperty() {
		return this.indexedAt;
	}
	
	public final LocalDateTime getIndexedAt() {
		return this.indexedAtProperty().get();
	}
	
	public final void setIndexedAt(final LocalDateTime indexedAt) {
		this.indexedAtProperty().set(indexedAt);
	}
	
    public String getName(){
    	return this.name;
    }
    
    
    public Document(){
    	this.path = new SimpleStringProperty();
		this.appear = new SimpleIntegerProperty();
		this.view = new SimpleIntegerProperty();
		this.length = new SimpleIntegerProperty();
		this.indexedAt = new SimpleObjectProperty<LocalDateTime> ();
    }
    
    public FileType getFileType(){
    	try{
    		return FileType.valueOf( name.substring(name.lastIndexOf('.')+1).toUpperCase() );
    	}catch(Exception e){
    		return null;
    	}
    }

}
