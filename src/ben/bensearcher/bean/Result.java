package ben.bensearcher.bean;

import java.io.Serializable;
import java.sql.Date;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@SuppressWarnings("serial")
public class Result implements Serializable{

	private ObjectProperty<Search> search;
	private ObjectProperty<Document> document;
	private ObjectProperty<Date> datetime;
	private StringProperty description; 
	/**
	 * @param search
	 * @param document
	 * @param datetime
	 * @throws BeanException 
	 */
	public Result(Search search, Document document, Date datetime, String description) throws BeanException {
		if( search != null && document != null && datetime != null){
			this.search = new SimpleObjectProperty<Search>(search);
			this.document = new SimpleObjectProperty<Document>(document);
			this.datetime = new SimpleObjectProperty<Date>(datetime);
			this.description = new SimpleStringProperty(description);
		}else{
			throw new BeanException("search, document or datetime is(are) null(s)");
		}
		
	}
	public final ObjectProperty<Search> searchProperty() {
		return this.search;
	}
	
	public final Search getSearch() {
		return this.searchProperty().get();
	}
	
	public final void setSearch(final Search search) {
		this.searchProperty().set(search);
	}
	
	public final ObjectProperty<Document> documentProperty() {
		return this.document;
	}
	
	public final Document getDocument() {
		return this.documentProperty().get();
	}
	
	public final void setDocument(final Document document) {
		this.documentProperty().set(document);
	}
	
	public final ObjectProperty<Date> datetimeProperty() {
		return this.datetime;
	}
	
	public final Date getDatetime() {
		return this.datetimeProperty().get();
	}
	
	public final void setDatetime(final Date datetime) {
		this.datetimeProperty().set(datetime);
	}
	
	public final StringProperty descriptionProperty() {
		return this.description;
	}
	
	public final String getDescription() {
		return this.descriptionProperty().get();
	}
	
	public final void setDescription(final String description) {
		this.descriptionProperty().set(description);
	}
}
