package ben.bensearcher.view;

import ben.bensearcher.bean.Result;
import ben.bensearcher.controller.ItemController;
import javafx.scene.control.ListCell;

public class ListViewCell extends ListCell<Result>
{
    @Override
    public void updateItem(Result result, boolean empty)
    {
        super.updateItem(result,empty);
        if(result != null)
        {
        	ItemController data = new ItemController();
            data.setInfo(result);
            setGraphic(data.getBox());
        }
    }

}
