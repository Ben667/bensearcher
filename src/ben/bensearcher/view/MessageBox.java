package ben.bensearcher.view;

import java.awt.Toolkit;

import ben.bensearcher.MainApp;
import ben.bensearcher.util.AppConst;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MessageBox {


	private static Object transitor;

	


	public static boolean confirm(String title, String content){
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(new Runnable(){
				public void run(){
					transitor = confirm(title,content);
				}
			});
			return (boolean)transitor;
		}
		Alert alert = new Alert(AlertType.CONFIRMATION,"",ButtonType.YES,ButtonType.NO);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(content);
		makeIcon(alert);
		alert.showAndWait();

		return (alert.getResult() == ButtonType.YES);
	}



	public static boolean confirmWithHeader(String title, String header, String content){
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(() -> {
				transitor = confirmWithHeader(title,header,content);	
			});
			return (boolean)transitor;
		}
		Alert alert = new Alert(AlertType.CONFIRMATION,"",ButtonType.OK,ButtonType.CANCEL);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.getDialogPane().setMaxWidth(Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2.5);
		makeIcon(alert);
		alert.showAndWait();

		return (alert.getResult() == ButtonType.OK);
	}




	public static void helpBox(){
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(() -> {
				helpBox();	
			});
			return;
		}
		String title = AppConst.APP_NAME + " - help";
		String header = "\t\tApplication help";
		String content = 
				"This sofware make you life easy with documents by help you to find them with "+
				"a contained word (named keyword in this app) from a chosen folder, just type "+
				"your keyword in text field, press Search button and wait; application will "+
				"recursively find typed keyword in any documents (of chosen type) from chosen root path."+
				"\n\t --- APPLICATION FUNCTIONS ---\n"+
				"Keyword textfield  \t->\t word to find into documents"+"\n"+
				"Search/stop button \t->\t start/stop searching (from chosen root path)"+"\n"+
				"File type          \t\t->\t set PDF for PDFs, TXT for *.txt file and ALL for both"+"\n"+
				"Root path          \t->\t choose folder in which the search will start into"+"\n"+
				"Limit result at    \t->\t set max number of result you want, set -1 for no limit"+"\n"+
				"Setting Menu       \t->\t Here you set default values for future launch "+"\n"+
				"Search Menu - Pause/Resume    \t->\t pause and resume searching process"+"\n"+
				"Search Menu - Recent searches \t->\t See your "+AppConst.MAX_RECENT_SEARCHES+" lasted searches" +"\n"+
				"\nYour can see about menu for information about application."+
				"\nWe thank you "+AppConst.USER_NAME+" to using this application";
		Alert alert = new Alert(AlertType.INFORMATION,"",ButtonType.OK);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.getDialogPane().setPrefWidth(550);
		alert.getDialogPane().setPrefHeight(430);
		makeIcon(alert);
		alert.showAndWait();
	}


	public static void information(String title, String content){
		alert(title,content,AlertType.INFORMATION);
	}


	public static void error(String title, String content){
		alert(title,content,AlertType.ERROR);
	}

	public static void warning(String title, String content){
		alert(title,content,AlertType.WARNING);
	}

	public static void none(String title, String content){
		alert(title,content,AlertType.NONE);
	}

	public static void alert(String title, String content, AlertType type){
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(new Runnable(){
				public void run(){
					alert(title,content,type);
				}
			});
			return;
		}
		Alert alert = new Alert(type,"",ButtonType.OK);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(content);
		alert.getDialogPane().setMaxWidth(Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2.5);
		makeIcon(alert);
		alert.showAndWait();
	}
	
	
	
	/**
	 * show an input dialog and return the user input.
	 * @param params in order are title,defaultText,content,headerText
	 * @return user input or null if the dialog was cancelled
	 */
	@SuppressWarnings("unused")
	public static String input(String... params){
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(new Runnable(){
				public void run(){
					transitor = input(params);
				}
			});
			return (String)transitor;
		}

		String title="",defaultText="",content="",headerText="";
		if(params != null){
			if(params.length >= 1)
				title = params[0];
			if(params.length >= 2)
				defaultText = params[1];
			if(params.length >= 3)
				content = params[2];
			if(params.length >= 4)
				headerText = params[3];
		}
		BorderPane root = new BorderPane();
		Text messageText = new Text(content);
		TextField textField = new TextField(defaultText);
		root.setTop(messageText);
		root.setCenter(textField);
		Dialog<String> dialog = new Dialog<>();

		makeIcon(dialog);
		ButtonType okButton = new ButtonType("OK",ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(okButton,ButtonType.CANCEL);

		//Node okBut = dialog.getDialogPane().lookupButton(okButton);
		dialog.getDialogPane().setContent(root);
		dialog.setResultConverter(dialogButton -> {
			if(dialogButton == okButton)
				return textField.getText();
			else
				return null;
		});

		return dialog.showAndWait().orElse(null);
	}
	
	
	
	public static void aboutBox(){
		
		if(!Platform.isFxApplicationThread()){
			Platform.runLater(() -> {
				aboutBox();
			});
			return;
		}
		
		String strings[] = {
				AppConst.APP_NAME,	//appName
				"Version 1.0, first release",	//version
				"MEZATSONG TSAFACK Carrel",	//author
				"(c) Copyright meztsacar@gmail.com 2018.  All rights reserved. ",	//copyright
				/*description - begin*/
				AppConst.APP_NAME+" has been make to help everybody who currently work with "+
						"documents such as Portable Document Format (PDF), it will help them for looking a document by content."
				/*description - end*/	
		};
		HBox mainHBox = new HBox(20);
		ImageView logo = new ImageView();
		VBox leftVBox = new VBox(10);
		Label appName = new Label(strings[0]);
		appName.setFont(new Font("Arial", 20));
		Label version = new Label(strings[1]);
		Label author = new Label(strings[2]);
		Label copyright = new Label(strings[3]);
		Text description = new Text(strings[4]);
		description.setWrappingWidth(500);
		
		leftVBox.getChildren().addAll(appName,version,author,copyright,description);
		mainHBox.getChildren().addAll(logo,leftVBox);
		mainHBox.setPrefWidth(40);
		
		Dialog<Void> dialog = new Dialog<>();

		makeIcon(dialog);
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
		dialog.getDialogPane().setContent(mainHBox);
		dialog.showAndWait();
	}


	@SuppressWarnings("rawtypes")
	private static void makeIcon(Dialog dialog) {
		try{
			Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
			stage.getIcons().add(new Image(MainApp.class.getResourceAsStream("view/icon.ico")));
		}catch(NullPointerException e){}
	}

	
}
