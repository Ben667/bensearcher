/**
 *
 */
package ben.bensearcher.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;

/**
 * @author Ben Mezatsong
 *
 */
public class QueryBuilder {

	/**
	 *
	 */
	private QueryBuilder() {}

	
	
	/**
	 * 
	 * @param query, prepared query
	 * @param params parameters for query
	 * @return ResultSet or Integer depending or query type.
	 */
	public static Object execute(String query, Object... params){
		boolean update = false;
		String updateWord[] = {"DELETE","UPDATE","INSERT"};
		try {
			for(String str: updateWord){
				if(query.toLowerCase().contains(str.toLowerCase())){
					update = true;
				}
			}
			
			PreparedStatement statement = MezConnection.getInstance().prepareStatement(query);
			
			if(params != null){
				for(int i=0; i<params.length; i++){
					statement.setObject(i, params[i]);
				}
			}
			
			if(update){
				return new Integer(statement.executeUpdate());
			}else{
				return statement.executeQuery();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	protected String specialChars(String str){
		if(str==null)
			return null;
		char unwantedCharsTab[] = {
				'\'','\"','`'
		};
		Vector<Character> unwantedChars = new Vector<Character>();
		char newStr[] = new char[ str.length() ];

		for(char c: unwantedCharsTab){
			unwantedChars.add(c);
		}
		int i=0;
		for(char c: str.toCharArray()){
			if(!unwantedChars.contains(c)){
				newStr[i++] = c;
			}else{
				newStr[i++] = '_';
			}
		}
		return String.valueOf(newStr);
	}
}
