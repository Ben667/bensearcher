/**
 *
 */
package ben.bensearcher.database;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author MEZATSONG TSAFACK Carrel
 *
 */
public class MezConnection {

	private final String DB_NAME = "data"+File.separator+"bensearcher.data";
	private String url = "jdbc:sqlite:"+DB_NAME;
	private static Connection connect = null;

	private MezConnection() {
		try {
			connect = DriverManager.getConnection(url);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Connection getInstance() {
		if(connect == null){
			synchronized(MezConnection.class){
				new MezConnection();
			}
		}
		return connect;
	}

}
