package ben.bensearcher.controller;

import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ben.bensearcher.bean.Document;
import ben.bensearcher.bean.Result;
import ben.bensearcher.util.AppConst;
import ben.bensearcher.util.FileType;
import ben.bensearcher.view.MessageBox;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ItemController
{
    @FXML private HBox hBox;
    @FXML private ImageView image;
    @FXML private Label label;
    @FXML private Hyperlink link;
    @FXML private TextFlow textFlow;
    @FXML private Text textBefore;
    @FXML private Text textMotif;
    @FXML private Text textAfter;

    public ItemController()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/listCellItem.fxml"));
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setInfo(Result result)
    {
        Document doc = result.getDocument();
        String description = result.getDescription();
        String motif = result.getSearch().getMotif();
        String strBefore = description.substring(0, description.indexOf(motif));
        String strAfter = description.substring( description.indexOf(motif) + motif.length());
        
        if(doc.getFileType() == FileType.PDF){
        	//image pdf
        }
        if(doc.getFileType() == FileType.TXT){
        	//image txt
        }
        label.setText(doc.getName());
        label.setTooltip(new Tooltip("Click to open"));
        link.setText(doc.getPath());
        link.setTooltip( new Tooltip("Show in folder") );
        textBefore.setText(strBefore);
        textMotif.setText(motif);
        textAfter.setText(strAfter);
       
        link.setOnAction(e -> { openFile( new File(doc.getPath()).getParentFile() ); });
        
        //ContextMenuEvent
        ContextMenu contextMenu = new ContextMenu();
        MenuItem openMenuItem = new MenuItem("Open");
        MenuItem copyMenuItem = new MenuItem("Copy path");
        MenuItem showInFolderMenuItem = new MenuItem("Show in folder");
        contextMenu.getItems().addAll(openMenuItem,new SeparatorMenuItem(),
        			copyMenuItem, new SeparatorMenuItem(), showInFolderMenuItem);
        
        openMenuItem.setOnAction( e -> { openFile(new File(doc.getPath())); });
        
        copyMenuItem.setOnAction(e -> {
        	Map<DataFormat,Object> map = new HashMap<>();
        	map.put(DataFormat.PLAIN_TEXT, doc.getPath());
        	Clipboard.getSystemClipboard().setContent(map);
        });
        
        showInFolderMenuItem.setOnAction( e -> { openFile(new File(doc.getPath()).getParentFile()); });
    
        
        link.setContextMenu(contextMenu);
        
        hBox.setOnMouseClicked( e -> { 
        	if(e.getButton() == MouseButton.PRIMARY)
        		openFile(new File(doc.getPath()));
        	else if (e.getButton() == MouseButton.SECONDARY){
        		contextMenu.show(hBox, e.getScreenX(), e.getScreenY());
        	}
        		
        });
        
        
        
    }

    public HBox getBox()
    {
        return hBox;
    }
    
    
    private void openFile(File file){
    	EventQueue.invokeLater(() -> {
    		if(Desktop.isDesktopSupported()){
        		try {
    				if(Desktop.getDesktop().isSupported(Action.OPEN))
    					Desktop.getDesktop().open(file);
    				else{
    					Platform.runLater(() -> {
    						MessageBox.warning(AppConst.APP_NAME, 
        							"Sorry we can't open this file");
    					});
    				}
    			} catch (Exception e1) {
    				e1.printStackTrace();
    			}
        	}
    	});
    }
    
    
}