package ben.bensearcher.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

import ben.bensearcher.bean.Document;
import ben.bensearcher.bean.Result;
import ben.bensearcher.bean.Search;
import ben.bensearcher.util.AppConst;
import ben.bensearcher.util.FileType;
import ben.bensearcher.util.Setting;
import ben.bensearcher.util.TextExtractor;
import ben.bensearcher.view.ListViewCell;
import ben.bensearcher.view.MessageBox;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Controller implements Initializable{
	
	//MenuBar
	@FXML private MenuItem threadControlMenuItem;
	@FXML private MenuItem fileTypeMenuItem;
	@FXML private MenuItem rootPathMenuItem;
	@FXML private MenuItem resultLimitMenuItem;
	@FXML private Menu recentMenu;
	
	//AppContent
	@FXML private ComboBox<FileType> fileType;
	@FXML private TextField rootPathTextField;
	@FXML private Button rootPathButton;
	@FXML private Spinner<Double> resultLimit;
	@FXML private TextField motifTextField;
	@FXML private Button action;
	@FXML private ListView<Result> listView;
	
	//Bottom
	@FXML private Label currentFileText;

	private StringProperty windowTitle;
	private boolean threadPause = false;
	private SearchService searchService;
	private final int NUMBER_OF_DESCRIPTION_CHARS = 150;
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		fileType.getItems().addAll(FileType.values());
		try {
			Setting setting = Setting.load();
			fileType.getSelectionModel().select(setting.getFileType());
			rootPathTextField.setText(setting.getRootPath());
			resultLimit.getValueFactory().setValue((double)setting.getResultLimit());
		} catch (Exception e) {
			e.printStackTrace();
		}
		threadControlMenuItem.setDisable(true);
		
		//listView
		listView.setCellFactory(new Callback<ListView<Result>, ListCell<Result>>()
        {
            @Override
            public ListCell<Result> call(ListView<Result> listView)
            {
                return new ListViewCell();
            }
        });
		
		currentFileText.setText("Hello "+AppConst.USER_NAME+" we thank you to use "+AppConst.APP_NAME+" !");
		
		initRecentSearches();
	}
	
	
	public void initRecentSearches(){
		Scanner scanner = null;
		try{
			scanner = new Scanner(AppConst.RECENT_FILE);
			recentMenu.getItems().clear();
			int count = 0;
			while(scanner.hasNextLine() && ++count <= AppConst.MAX_RECENT_SEARCHES){
				String str = scanner.nextLine();
				MenuItem menuItem = new MenuItem(String.valueOf(count)+". "+str);
				menuItem.setOnAction(e -> {
					stopingAction();
					motifTextField.setText(str);
					action();
				});
				recentMenu.getItems().add(menuItem);
			}
		}catch(Exception e){ e.printStackTrace(); }
		finally{
			if(scanner != null)
				scanner.close();
		}
	}
	
	
	private void addNewWordToRecentList(){
		List<String> newLines = new ArrayList<String>();
		List<String> sentinel = new ArrayList<String>(); 
		newLines.add(motifTextField.getText());
		
		Scanner scanner = null;
		FileWriter writer = null;
	
		try{
			scanner = new Scanner(AppConst.RECENT_FILE);
			int count = 0;
			//we add +1 (because it must be <) for prevent the case where the keyword already exist
			while(scanner.hasNextLine() && ++count <= AppConst.MAX_RECENT_SEARCHES){
				String str = scanner.nextLine();
				if(!str.isEmpty())
					newLines.add( str );
			}
			
			
			writer = new FileWriter(AppConst.RECENT_FILE);
			recentMenu.getItems().clear();
			int number = 0;
			for(int i=0; i < newLines.size() && i<AppConst.MAX_RECENT_SEARCHES; i++){
				
				String str = newLines.get(i);
				
				if(sentinel.contains(str)){
					continue;
				}else{
					sentinel.add(str);
				}
				
				writer.write(str+"\n");
			
				MenuItem menuItem = new MenuItem(String.valueOf(++number)+". "+str);
				menuItem.setOnAction(e -> {
					motifTextField.setText(str);
					action();
				});
				recentMenu.getItems().add(menuItem);
			}
			
			writer.flush();
		}catch(Exception e){}
		finally{
			if(scanner != null)
				scanner.close();
			if(writer != null)
				try {
					writer.close();
				} catch (IOException e) {}
		}
	}
	
	
	public void rootPathButton(){
		File initialDir = new File(rootPathTextField.getText());
		DirectoryChooser rootPathChooser = new DirectoryChooser();
		rootPathChooser.setTitle("Select search root path");
		if(!initialDir.exists() || !initialDir.isDirectory()){
			initialDir = new File( AppConst.HOME_DIR );
			rootPathTextField.setText(initialDir.getAbsolutePath());
		}
		rootPathChooser.setInitialDirectory(initialDir);
		File rootPath = rootPathChooser.showDialog(null);
		if(rootPath != null && rootPath.isDirectory()){
			rootPathTextField.setText(rootPath.getAbsolutePath());
		}
	}
	
	
	
	private void stopingAction(){
		String str = action.getText();
		if( str.equals("Stop") ){
			threadPause = false;
			pauseResume();
			searchService.cancel();
			//searchService = null;
			action.setText("Search");
			currentFileText.setText("");
			windowTitle.set(AppConst.APP_NAME);
			setCurrentSettingDisabled(false);
			threadControlMenuItem.setText("Pause/Resume");
			threadControlMenuItem.setDisable(true);
		}
	}
	
	private void startingAction(){
		if(searchService == null){
			searchService = new SearchService();
		}
		threadPause = false;
		action.setText("Stop");
		windowTitle.set(AppConst.APP_NAME + " - " + motifTextField.getText() );
		threadControlMenuItem.setText("Pause");
		threadControlMenuItem.setDisable(false);
		setCurrentSettingDisabled(true);
		listView.getItems().clear();
		searchService.restart();
	}
	
	public void action(){
		if(windowTitle == null){
			Stage stage = (Stage) currentFileText.getScene().getWindow();
			windowTitle = stage.titleProperty();
		}
		String str = action.getText();
		if( str.equals("Search") ){
			startingAction();
			addNewWordToRecentList();
		}else{
			stopingAction();
		}
	}
	
	
	
	
	public void pauseResume(){
		if( !threadControlMenuItem.isDisable() && searchService != null){
			threadPause = !threadPause;
			if( threadPause ){
				threadControlMenuItem.setText("Resume");
				currentFileText.setText(" - Paused - ");
				searchService.suspend();
			}else{
				threadControlMenuItem.setText("Pause");
				currentFileText.setText(" Resuming... ");
				searchService.resume();
			}
		}
	}


		
	public void fileTypeFocus(){
		fileType.requestFocus();
		fileType.show();
	}
	
	
	public void resultLimitFocus(){
		resultLimit.requestFocus();
	}
	
	
	public void exitAction(){
		if(searchService != null){
			searchService.cancel();
		}
		Platform.exit();
	}
	
	private void setCurrentSettingDisabled(boolean bool){
		fileType.setDisable(bool);
		rootPathButton.setDisable(bool);
		rootPathTextField.setDisable(bool);
		resultLimit.setDisable(bool);
		
		fileTypeMenuItem.setDisable(bool);
		rootPathMenuItem.setDisable(bool);
		resultLimitMenuItem.setDisable(bool);
		
		recentMenu.setDisable(bool);
	}
	
	
	public void defaultRootPath(){
		Setting setting;
		try {
			setting = Setting.load();
		} catch (Exception e) {
			MessageBox.error(AppConst.APP_NAME+" - error",
					"There was error reading setting file: "+e.getMessage());
			return;
		}
		File initialDir = new File(setting.getRootPath());
		DirectoryChooser rootPathChooser = new DirectoryChooser();
		rootPathChooser.setTitle("Select default search root path");
		if(!initialDir.exists() || !initialDir.isDirectory()){
			initialDir = new File( AppConst.HOME_DIR );
			rootPathTextField.setText(initialDir.getAbsolutePath());
		}
		rootPathChooser.setInitialDirectory(initialDir);
		File rootPath = rootPathChooser.showDialog(null);
		boolean status = false;
		String title = AppConst.APP_NAME;
		String message = "Sorry the was an error !";
		if(rootPath != null && rootPath.isDirectory()){
			setting.setRootPath(rootPath.getAbsolutePath());
			if(setting.save()){
				message = "Setting saved successfully!\n"+rootPath.getAbsolutePath()
				+"\nis now default root path";
				status = true;
			}else{
				message = "There was error while saving setting file";
			}
		}else{
			message = "Please choose a valid path!";
		}
		
		if(status){
			MessageBox.information(title, message);
		}else{
			title += " - error";
			MessageBox.error(title, message);
		}
	}
	
	
	public void defaultResultLimit(){
		String title = AppConst.APP_NAME + " - Default result limit";
		String message = "Enter default result limit, it must be an integer\nEnter -1 for infinity.";
		
		try{
			Setting setting = Setting.load();
			String limit = MessageBox.input(title,String.valueOf(setting.getResultLimit()),message);
			
			if(limit == null)
				return;
			int intLimit = Double.valueOf(limit).intValue();
			if (intLimit < -1 || intLimit > 500){
				MessageBox.error(title, 
						"Please the number must be lower or equals to 500 and greather or equals to -1");
			}else{
				
				setting.setResultLimit(intLimit);
				if(setting.save()){
					message = "Setting saved successfully!\n"
					+intLimit+" is now default result limit";
					MessageBox.information(title, message);
				}else{
					message = "There was error while saving setting file";
					MessageBox.error(title, message);
				}
			}
		}catch(NumberFormatException e){
			MessageBox.error(title, "Please the entry must be a integer number");
		}catch(Exception e){
			MessageBox.error(title,"There was error reading setting file: "+e.getMessage());
		}
	}
	
	
	public void defaultFileType(){
		String title = AppConst.APP_NAME + " - Default file type";
		String message = "Enter default file type (\"ALL\" for all type).";
		String entry = MessageBox.input(title,fileType.getValue().name(),message);
		try{
			if(entry == null || entry.isEmpty() ) return;
			if(entry.contains("*")){
				entry = entry.replace("*", "");
			}
			if(entry.contains(".")){
				entry = entry.replace(".", "");
			}
			entry = entry.toUpperCase();
			FileType type = null;
			try{
				type = FileType.valueOf(entry);
			}catch(Exception e){ type = null;}
			if(type != null){
				Setting setting = Setting.load();
				setting.setFileType(type);
				if(setting.save()){
					message = "Setting saved successfully!\n"
					+type.name()+" is now default file type";
					MessageBox.information(title, message);
				}else{
					message = "There was error while saving setting file";
					MessageBox.error(title, message);
				}
			}else{
				title += " - error";
				message = entry+" value is not accepted!\n";
				message += "Here is accepted values:";
				for(FileType ft : FileType.values()){
					message += "\n\t"+ft.name();
				}
				MessageBox.error(title, message);
			}
		}catch(Exception e){
			MessageBox.error(title,"There was error reading setting file: "+e.getMessage());
		}
	}
	
	
	public void help(){
		MessageBox.helpBox();
	}
	
	
	public void about(){
		MessageBox.aboutBox();
	}

	
	class SearchService extends Service<Void>{
		SearchTask task;
		
		@Override
		protected Task<Void> createTask() {
			task = new SearchTask();
			return task;
		}
		
		public void suspend(){
			task.suspend();
		}
		
		
		public void resume(){
			task.resume();
		}
		
		@Override
		public boolean cancel(){
			task.stop();
			return super.cancel();
		}
	}
	
	
	class SearchTask extends Task<Void>{

		private String motif;
		private File root;
		private FileType type;
		private int limit;
		private Search search;
		private Boolean doPause;
		
		SearchTask(){
			doPause = Boolean.FALSE;
		}
		
		@Override
		protected Void call() throws Exception {
			listView.getItems().clear();
			motif = motifTextField.getText();
			root = new File( rootPathTextField.getText() );
			type = fileType.getValue();
			try{
				limit = resultLimit.getValue().intValue();
			}catch(Exception e){
				limit = Double.valueOf(resultLimit.getEditor().getText()).intValue();
			}
			//verification
			if(limit < -1 || limit > 500){
				MessageBox.error(AppConst.APP_NAME+" - error", 
						"Please the limit must be lower or equals to 500 and greather or equals to -1");
				action();
				return null;
			}
			if(motif == null || motif.isEmpty()){
				MessageBox.error(AppConst.APP_NAME+" - error", 
						"Search motif can't be empty");
				action();
				return null;
			}
			if(root == null || !root.exists() || !root.isDirectory()){
				MessageBox.error(AppConst.APP_NAME+" - error", 
						"Invalide root path");
				action();
				return null;
			}
			if(limit == -1)
				limit = Integer.MAX_VALUE;
			search = new Search();
			search.setMotif(motif);
			search.setFileType(type);
			search.setDatetime(new Date(System.currentTimeMillis()));
			search.setResultLimit(limit);
			search.setRootPath(root.getAbsolutePath());
			
			//start search from root path
			try{
				search(root);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			action();
			
			return null;
		}

		private void search(File file) throws Exception {
			if(limit <= 0 || isCancelled()){
				return;
			}
			while(doPause){
				Thread.sleep(500);
			}
			Platform.runLater(new Runnable(){
				@Override
				public void run() { 
					currentFileText.setText("Looking for \""+motif+"\" in "+file.getAbsolutePath()); 
				}
			});
			if(file.isFile()){
				if(isValid(file)){
					Document doc = new Document();
					doc.setPath(file.getAbsolutePath());
					StringBuilder strB = new StringBuilder();
					try{
						strB = TextExtractor.extract(doc,motif);
					}catch(Exception e){}
					int pos = strB.indexOf(motif);
					if(pos != -1){
						int length = strB.length();
						int min = pos - NUMBER_OF_DESCRIPTION_CHARS/2;
						int max = pos + NUMBER_OF_DESCRIPTION_CHARS/2;
						if(min < 0){
							max += Math.abs(min);
							min = 0;
						}
						if(max >= length){
							min -= max - length;
							max = length-1;
						}
						min = Math.max(0, min);
						max = Math.min(max, length-1);
						String description = strB.substring(min,max);
						Result result = new Result(search,doc,
								new Date(System.currentTimeMillis()),description);
						
						Platform.runLater(new Runnable(){
							@Override
							public void run() { listView.getItems().add(result); }
						});
						limit--;
					}
				}
			}else{
				File tab[] = file.listFiles();
				if(tab != null && tab.length > 0){
					for(File f : tab){
						if(limit > 0 && !isCancelled())
							search(f);
					}
				}
			}
		}
		
		
		private void action(){
			Platform.runLater(new Runnable(){

				@Override
				public void run() {
					listView.refresh();
					Controller.this.stopingAction();
				}
				
			});
		}
		
		
		private boolean isValid(final File file){
			String name = file.getName().toUpperCase();
			if(type != FileType.ALL){
				if(name.endsWith(type.name()))
					return true;
			}else{
				for(FileType t : FileType.values()){
					if(name.endsWith(t.name()) && t != FileType.ALL)
						return true;
				}
			}
			return false;
		}
		
		
		public void suspend(){
			doPause = true;
		}
		
		
		public void resume(){
			doPause = false;
		}
		
		
		public void stop(){
			limit = 0;
		}
	
	}
	
	
}
